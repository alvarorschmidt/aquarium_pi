#!/usr/bin/env python3
import os

from time import sleep
from ui import Ui


def git_routine():
    os.system("git pull")
    sleep(5)


class Aquarium:
    def __init__(self):
        self.ui = Ui("Aquarium!")
        self.ui.mainloop()


if __name__ == "__main__":
    git_routine()
    app = Aquarium()
