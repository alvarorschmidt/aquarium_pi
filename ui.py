try:
    import tkinter as tkinter
except:
    import Tkinter as tkinter


class Ui(tkinter.Frame):
    def __init__(self, window_title=None):
        self.root = tkinter.Tk()
        tkinter.Frame.__init__(self, self.root)
        self.winfo_toplevel().title(window_title)

    def mainloop(self):
        self.root.mainloop()
